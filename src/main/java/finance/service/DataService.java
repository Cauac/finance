package finance.service;

import finance.dao.ExpenseDAO;
import finance.dao.IncomeDAO;
import finance.wrappers.Expense;
import finance.wrappers.Income;

import java.time.LocalDate;
import java.util.*;

public class DataService {

    private static DataService instance = new DataService();
    private ExpenseDAO expenseDAO = new ExpenseDAO();
    private IncomeDAO incomeDAO = new IncomeDAO();

    public static DataService getInstance() {
        return instance;
    }

    public ExpenseDAO getExpenseDAO() {
        return expenseDAO;
    }

    public IncomeDAO getIncomeDAO() {
        return incomeDAO;
    }

    public long getIncomeSum() {
        return incomeDAO.getSum();
    }

    public Map<String, Long> getSumByType() {
        Map<String, Long> sumByType = new HashMap<>();
        for (String type : Expense.Type.getTypeList()) {
            sumByType.put(type, 0l);
        }
        sumByType.putAll(expenseDAO.getSumByType());
        return sumByType;
    }

    public List<Income> getIncomeByMonth(LocalDate date) {
        return incomeDAO.readByDate(date);
    }

    public List<Expense> getExpenseByMonth(LocalDate date) {
        return expenseDAO.readByDate(date);
    }


    public Map<String, Long> getSumByTypeByMonth(LocalDate date) {
        Map<String, Long> sumByType = new HashMap<>();
        for (String type : Expense.Type.getTypeList()) {
            sumByType.put(type, 0l);
        }
        sumByType.putAll(expenseDAO.getSumByTypeByMonth(date));
        return sumByType;
    }

    public long getIncomeSum(LocalDate date) {
        return incomeDAO.getSum(date);
    }

    public Map<String, Map> getExpenseByTypeAndMonth() {
        return expenseDAO.getExpenseByTypeAndMonth();
    }
}
