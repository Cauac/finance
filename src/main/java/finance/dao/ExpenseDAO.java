package finance.dao;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import finance.wrappers.Expense;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExpenseDAO extends MongoDAO {

    private ExpenseFilter filter = new ExpenseFilter();

    public ExpenseDAO() {
        super("expense");
    }

    public List<Expense> readAll() {
        DBCursor cursor = collection.find();
        List<Expense> result = new ArrayList();
        while (cursor.hasNext()) {
            result.add(new Expense(cursor.next()));
        }
        return result;
    }

    public List<Expense> readByDate(LocalDate date) {
        BasicDBList ids = filter.filter(date);
        DBCursor cursor = collection.find(new BasicDBObject("_id", new BasicDBObject("$in", ids)));
        List<Expense> result = new ArrayList();
        while (cursor.hasNext()) {
            result.add(new Expense(cursor.next()));
        }
        return result;
    }

    public void save(Expense expense) {
        collection.insert(expense.getRecord());
        filter.add(expense);
    }

    public void update(Expense expense) {
        filter.remove(loadOldValue(expense));
        filter.add(expense);
        collection.save(expense.getRecord());
    }

    public void delete(Expense expense) {
        filter.remove(expense);
        collection.remove(expense.getRecord());
    }

    public Map<String, Long> getSumMap() {
        return filter.getSumMap();
    }

    public Map<String, Long> getSumByType() {
        return filter.getSumByType();
    }

    public Map<String, Long> getSumByTypeByMonth(LocalDate date) {
        return filter.getSumByType(date);
    }

    public Map<String, Map> getExpenseByTypeAndMonth() {
        return filter.getExpenseByTypeAndMonth();
    }

    private Expense loadOldValue(Expense expense) {
        Object id = expense.getRecord().get("_id");
        return new Expense(collection.findOne(id));
    }
}
