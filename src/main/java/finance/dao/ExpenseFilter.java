package finance.dao;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import finance.FinanceUtils;
import finance.wrappers.Expense;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class ExpenseFilter extends MongoDAO {

    protected ExpenseFilter() {
        super("expense-filter");
    }

    public void add(Expense expense) {
        Object id = expense.getRecord().get("_id");
        String type = expense.getType();
        long money = expense.getMoney();
        String date = expense.getDate().substring(0, 7);
        collection.update(new BasicDBObject("_id", date), new BasicDBObject("$push", new BasicDBObject("expenses." + type, id)), true, false);
        collection.update(new BasicDBObject("_id", date), new BasicDBObject("$inc", new BasicDBObject("statistic.sum", money)), true, false);
        collection.update(new BasicDBObject("_id", date), new BasicDBObject("$inc", new BasicDBObject("statistic." + type, money)), true, false);
        collection.update(new BasicDBObject("_id", "statistic"), new BasicDBObject("$inc", new BasicDBObject("sum", money)), true, false);
        collection.update(new BasicDBObject("_id", "statistic"), new BasicDBObject("$inc", new BasicDBObject(type, money)), true, false);
    }

    public void remove(Expense expense) {
        Object id = expense.getRecord().get("_id");
        String type = expense.getType();
        long money = -expense.getMoney();
        String date = expense.getDate().substring(0, 7);
        collection.update(new BasicDBObject("_id", date), new BasicDBObject("$pull", new BasicDBObject("expenses." + type, id)), true, false);
        collection.update(new BasicDBObject("_id", date), new BasicDBObject("$inc", new BasicDBObject("statistic.sum", money)), true, false);
        collection.update(new BasicDBObject("_id", date), new BasicDBObject("$inc", new BasicDBObject("statistic." + type, money)), true, false);
        collection.update(new BasicDBObject("_id", "statistic"), new BasicDBObject("$inc", new BasicDBObject("sum", money)), true, false);
        collection.update(new BasicDBObject("_id", "statistic"), new BasicDBObject("$inc", new BasicDBObject(type, money)), true, false);
    }

    public BasicDBList filter(LocalDate date) {
        String id = FinanceUtils.dateFormater.format(date);
        DBObject object = collection.findOne(new BasicDBObject("_id", id), new BasicDBObject("expenses", 1));
        BasicDBList result = new BasicDBList();
        if (object != null && object.containsField("expenses")) {
            object.removeField("_id");
            DBObject expenses = (DBObject) object.get("expenses");
            for (String type : expenses.keySet()) {
                result.addAll((BasicDBList) expenses.get(type));
            }
        }
        return result;
    }

    public Map<String, Long> getSumMap() {
        Map<String, Long> result = new HashMap();
        DBCursor cursor = collection.find(null, new BasicDBObject("statistic", 1));
        DBObject next;
        while (cursor.hasNext()) {
            next = cursor.next();
            DBObject statistic = (DBObject) next.get("statistic");
            if (statistic != null) {
                result.put(next.get("_id").toString(), (Long) statistic.get("sum"));
            }
        }
        cursor.close();
        return result;
    }

    public Map<String, Long> getSumByType() {
        Map<String, Long> sumByType = new HashMap<>();
        DBObject statistic = collection.findOne(new BasicDBObject("_id", "statistic"));
        if (statistic != null) {
            statistic.removeField("_id");
            for (String key : statistic.keySet()) {
                sumByType.put(key, (Long) statistic.get(key));
            }
        }
        return sumByType;
    }

    public Map<String, Long> getSumByType(LocalDate date) {
        Map<String, Long> sumByType = new HashMap<>();
        String id = FinanceUtils.dateFormater.format(date);
        DBObject object = collection.findOne(new BasicDBObject("_id", id), new BasicDBObject("statistic", 1));
        if (object != null && object.containsField("statistic")) {
            DBObject statistic = (DBObject) object.get("statistic");
            for (String key : statistic.keySet()) {
                sumByType.put(key, (Long) statistic.get(key));
            }
        }
        return sumByType;
    }

    public Map<String, Map> getExpenseByTypeAndMonth() {
        Map<String, Map> result = new HashMap();
        DBCursor cursor = collection.find(null, new BasicDBObject("statistic", 1));
        DBObject next;
        while (cursor.hasNext()) {
            next = cursor.next();
            DBObject statistic = (DBObject) next.get("statistic");
            if (statistic != null) {
                result.put(next.get("_id").toString(), statistic.toMap());
            }
        }
        return result;
    }
}
