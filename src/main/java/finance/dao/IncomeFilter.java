package finance.dao;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import finance.FinanceUtils;
import finance.wrappers.Income;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class IncomeFilter extends MongoDAO {

    protected IncomeFilter() {
        super("income-filter");
    }

    public void add(Income income) {
        Object id = income.getRecord().get("_id");
        long money = income.getMoney();
        String date = income.getDate().substring(0, 7);
        collection.update(new BasicDBObject("_id", date), new BasicDBObject("$push", new BasicDBObject("incomes", id)), true, false);
        collection.update(new BasicDBObject("_id", date), new BasicDBObject("$inc", new BasicDBObject("sum", money)), true, false);
        collection.update(new BasicDBObject("_id", "statistic"), new BasicDBObject("$inc", new BasicDBObject("sum", money)), true, false);
    }

    public void remove(Income income) {
        Object id = income.getRecord().get("_id");
        long money = -income.getMoney();
        String date = income.getDate().substring(0, 7);
        collection.update(new BasicDBObject("incomes", id), new BasicDBObject("$pull", new BasicDBObject("incomes", id)), true, false);
        collection.update(new BasicDBObject("_id", date), new BasicDBObject("$inc", new BasicDBObject("sum", money)), true, false);
        collection.update(new BasicDBObject("_id", "statistic"), new BasicDBObject("$inc", new BasicDBObject("sum", money)), true, false);
    }

    public BasicDBList filter(LocalDate date) {
        String id = FinanceUtils.dateFormater.format(date);
        DBObject object = collection.findOne(new BasicDBObject("_id", id));
        if (object == null || !object.containsField("incomes")) {
            return new BasicDBList();
        }
        return (BasicDBList) object.get("incomes");
    }

    public long getSum() {
        DBObject statistic = collection.findOne(new BasicDBObject("_id", "statistic"));
        if (statistic == null) {
            return 0L;
        }
        return (long) statistic.get("sum");
    }

    public Map<String, Long> getSumMap() {
        Map<String, Long> result = new HashMap();
        DBCursor cursor = collection.find(null, new BasicDBObject("sum", 1));
        DBObject next;
        while (cursor.hasNext()) {
            next = cursor.next();
            result.put(next.get("_id").toString(), (Long) next.get("sum"));
        }
        cursor.close();
        return result;
    }

    public long getSum(LocalDate date) {
        String id = FinanceUtils.dateFormater.format(date);
        DBObject object = collection.findOne(new BasicDBObject("_id", id));
        if (object == null) {
            return 0L;
        }
        return (long) object.get("sum");
    }
}
