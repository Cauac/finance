package finance.dao;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import finance.wrappers.Income;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class IncomeDAO extends MongoDAO {

    private IncomeFilter filter = new IncomeFilter();

    public IncomeDAO() {
        super("income");
    }

    public List<Income> readAll() {
        DBCursor cursor = collection.find();
        List<Income> result = new ArrayList();
        while (cursor.hasNext()) {
            result.add(new Income(cursor.next()));
        }
        return result;
    }

    public List<Income> readByDate(LocalDate date) {
        BasicDBList ids = filter.filter(date);
        DBCursor cursor = collection.find(new BasicDBObject("_id", new BasicDBObject("$in", ids)));
        List<Income> result = new ArrayList();
        while (cursor.hasNext()) {
            result.add(new Income(cursor.next()));
        }
        return result;
    }

    public void save(Income income) {
        collection.insert(income.getRecord());
        filter.add(income);
    }

    public void update(Income income) {
        filter.remove(loadOldValue(income));
        filter.add(income);
        collection.save(income.getRecord());
    }

    public void delete(Income income) {
        filter.remove(income);
        collection.remove(income.getRecord());
    }

    public long getSum() {
        return filter.getSum();
    }

    public Map<String, Long> getSumMap() {
        return filter.getSumMap();
    }

    public long getSum(LocalDate date) {
        return filter.getSum(date);
    }

    private Income loadOldValue(Income income) {
        Object id = income.getRecord().get("_id");
        return new Income(collection.findOne(id));
    }
}
