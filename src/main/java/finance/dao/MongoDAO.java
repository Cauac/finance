package finance.dao;

import com.mongodb.*;

import java.io.IOException;
import java.net.UnknownHostException;

public class MongoDAO {

    private static MongoClient mongoClient;
    protected static DB database;
    protected DBCollection collection;

    static {
        try {
            Runtime rt = Runtime.getRuntime();
            rt.exec("D:\\Development\\Servers\\MongoDB\\2.6.0\\bin\\mongod.exe --dbpath=D:\\Development\\Servers\\MongoDB\\data\\finance");
            mongoClient = new MongoClient("127.0.0.1", 27017);
            database = mongoClient.getDB("finance-test");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected MongoDAO(String collectionName) {
        collection = database.getCollection(collectionName);
    }
}
