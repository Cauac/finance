package finance.wrappers;

import com.mongodb.DBObject;
import finance.FinanceUtils;
import javafx.beans.property.SimpleStringProperty;

public class FinanceTransaction {

    protected DBObject record;

    public class Fields {
        public static final String TITLE = "title";
        public static final String DESCRIPTION = "description";
        public static final String MONEY = "money";
        public static final String DATE = "date";
    }

    public FinanceTransaction(DBObject record) {
        this.record = record;
    }

    public DBObject getRecord() {
        return record;
    }

    public String getTitle() {
        return record.get(Fields.TITLE).toString();
    }

    public String getDescription() {
        return record.get(Fields.DESCRIPTION).toString();
    }

    public Long getMoney() {
        return (Long) record.get(Fields.MONEY);
    }

    public String getDate() {
        return record.get(Fields.DATE).toString();
    }

    public void setTitle(String title) {
        record.put(Fields.TITLE, title);
    }

    public void setDescription(String description) {
        record.put(Fields.DESCRIPTION, description);
    }

    public void setMoney(Long money) {
        record.put(Fields.MONEY, money);
    }

    public void setDate(String date) {
        record.put(Fields.DATE, date);
    }

    public SimpleStringProperty moneyProperty() {
        return new SimpleStringProperty(FinanceUtils.formatMoney(getMoney()));
    }
}
