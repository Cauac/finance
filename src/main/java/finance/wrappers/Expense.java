package finance.wrappers;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Expense extends FinanceTransaction {

    public Expense(DBObject record) {
        super(record);
    }

    public Expense() {
        super(new BasicDBObject() {{
            put(Fields.TITLE, "");
            put(Fields.DESCRIPTION, "");
            put(Fields.MONEY, 0L);
            put(Fields.DATE, DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now()));
            put(Fields.TYPE, Type.FOOD);
        }});
    }

    public String getType() {
        return record.get(Fields.TYPE).toString();
    }

    public void setType(String type) {
        record.put(Fields.TYPE, type);
    }

    public class Fields extends FinanceTransaction.Fields {
        public static final String TYPE = "type";
    }

    public static class Type {

        public static final String FOOD = "Еда";
        public static final String CLOTHES = "Одежда";
        public static final String PAYMENT = "Платежи";
        public static final String EQUIPMENT = "Техника";
        public static final String HOUSEHOLDS = "Хозяйственные товары";
        public static final String ENTERTAINMENT = "Развлечения/Хобби";
        public static final String COSMETICS = "Красота и здоровье";
        public static final String PRESENTS = "Подарки";
        public static final String TRIP = "Поездки";
        public static final String UNEXPECTED = "Непредвиденное";

        public static List<String> getTypeList() {
            return new ArrayList() {{
                add(FOOD);
                add(CLOTHES);
                add(PAYMENT);
                add(EQUIPMENT);
                add(HOUSEHOLDS);
                add(ENTERTAINMENT);
                add(COSMETICS);
                add(PRESENTS);
                add(TRIP);
                add(UNEXPECTED);
            }};
        }
    }
}
