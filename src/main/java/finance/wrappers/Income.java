package finance.wrappers;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Income extends FinanceTransaction {

    public Income() {
        super(new BasicDBObject() {{
            put(Fields.TITLE, "");
            put(Fields.DESCRIPTION, "");
            put(Fields.MONEY, 0L);
            put(Fields.DATE, DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now()));
        }});
    }

    public Income(DBObject record) {
        super(record);
    }
}
