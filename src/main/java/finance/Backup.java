package finance;

import finance.dao.ExpenseDAO;
import finance.dao.IncomeDAO;
import finance.wrappers.Expense;
import finance.wrappers.Income;

import java.io.*;
import java.util.Scanner;

public class Backup {
    public static void main(String[] args) throws IOException {
//        backUpIncome();
//        backUpExpense();
//        uploadIncome();
        uploadExpense();
    }

    private static void backUpExpense() throws IOException {
        File file = new File("expense");
        PrintWriter writer = new PrintWriter(file);
        ExpenseDAO dao = new ExpenseDAO();
        int count = 0;
        String template = "%s\t%s\t%s\t%s\t%s";
        for (Expense expense : dao.readAll()) {
            String out = String.format(template, expense.getTitle(), expense.getDescription(), expense.getType(), expense.getMoney(), FinanceUtils.dateFormat.format(expense.getDate()));
            writer.println(out);
            count++;
        }
        writer.close();
        System.out.println("Expense backup done");
        System.out.println("Total count: " + count);
    }

    private static void backUpIncome() throws IOException {
        File file = new File("income");
        PrintWriter writer = new PrintWriter(file);
        IncomeDAO dao = new IncomeDAO();
        int count = 0;
        String template = "%s\t%s\t%s\t%s";
        for (Income income : dao.readAll()) {
            String out = String.format(template, income.getTitle(), income.getDescription(), income.getMoney(), FinanceUtils.dateFormat.format(income.getDate()));
            writer.println(out);
            count++;
        }
        writer.close();
        System.out.println("Income backup done");
        System.out.println("Total count: " + count);
    }

    private static void uploadIncome() throws FileNotFoundException {
        IncomeDAO dao = new IncomeDAO();
        File file = new File("income");
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String[] fields = scanner.nextLine().split("\t");
            Income income = new Income();
            income.setTitle(fields[0]);
            income.setDescription(fields[1]);
            income.setMoney(Long.parseLong(fields[2]));
            income.setDate(fields[3]);
            dao.save(income);
        }
    }

    private static void uploadExpense() throws FileNotFoundException {
        ExpenseDAO dao = new ExpenseDAO();
        File file = new File("expense");
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String[] fields = scanner.nextLine().split("\t");
            Expense expense = new Expense();
            expense.setTitle(fields[0]);
            expense.setDescription(fields[1]);
            expense.setType(fields[2]);
            expense.setMoney(Long.parseLong(fields[3]));
            expense.setDate(fields[4]);
            dao.save(expense);
        }
    }
}
