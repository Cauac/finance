package finance;

import finance.view.RootPane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.SceneBuilder;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("Please wait...");
        Scene scene = SceneBuilder.create().root(new RootPane()).height(900).width(1000).fill(Color.BROWN).build();
        primaryStage.setTitle("Финансы");
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(primaryStage.getScene().getHeight());
        primaryStage.setMinWidth(primaryStage.getScene().getWidth());
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
