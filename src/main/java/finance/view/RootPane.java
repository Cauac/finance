package finance.view;

import finance.view.tab.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Side;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class RootPane extends VBox {

    public RootPane() {
        final TabPane tabPane = new TabPane();
        VBox.setVgrow(tabPane, Priority.ALWAYS);
        tabPane.setSide(Side.TOP);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        tabPane.getTabs().add(new ExpenseTab());
        tabPane.getTabs().add(new IncomeTab());
        tabPane.getTabs().add(new StatisticTab());
        tabPane.getTabs().add(new ChartsTab());
        tabPane.getTabs().add(new ChartsTab2());
        getChildren().add(tabPane);

        tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observableValue, Tab tab, Tab tab2) {
                LazyTab lazyTab = (LazyTab) tab2;
                lazyTab.select();
            }
        });
    }
}
