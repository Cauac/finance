package finance.view.dialog;

import finance.view.ViewStyle;
import finance.wrappers.Expense;
import finance.wrappers.FinanceTransaction;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;

public class ExpenseDialogScene extends IncomeDialogStage {

    ChoiceBox<String> type = new ChoiceBox();

    public ExpenseDialogScene(FinanceTransaction expense) {
        super(expense);
        type.getItems().addAll(Expense.Type.getTypeList());
        type.getSelectionModel().selectFirst();
        type.setMinWidth(280);
        Label label = ViewStyle.getStyledLabel("Тип");
        fields.getChildren().add(2, label);
        fields.getChildren().add(3, type);
    }

    @Override
    protected void fillModelFields() {
        super.fillModelFields();
        ((Expense) transaction).setType(type.getValue());
    }
}
