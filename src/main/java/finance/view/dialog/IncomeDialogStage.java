package finance.view.dialog;

import finance.view.ViewStyle;
import finance.wrappers.FinanceTransaction;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class IncomeDialogStage extends Stage {

    private boolean okPressed;

    protected final Stage instance = this;
    protected FinanceTransaction transaction;
    protected DatePicker dateField = new DatePicker();
    protected TextField titleField = ViewStyle.getStyledTextFiled();
    protected TextField moneyField = ViewStyle.getStyledTextFiled();
    protected TextField descriptionField = ViewStyle.getStyledTextFiled();
    protected Button ok = ViewStyle.getStyledMenuButton("OK");
    protected Button cancel = ViewStyle.getStyledMenuButton("Отмена");
    protected VBox fields = VBoxBuilder.create().spacing(10).build();

    public IncomeDialogStage(FinanceTransaction transaction) {
        this.transaction = transaction;
        build();
    }

    public boolean isOK() {
        return okPressed;
    }

    protected void build() {
        StackPane roopGroup = new StackPane();
        roopGroup.setStyle("-fx-padding: 5px");
        VBox parent = VBoxBuilder.create().spacing(10).alignment(Pos.CENTER).fillWidth(true).build();
        roopGroup.getChildren().addAll(ViewStyle.getDialogBackground(300, 450), parent);
        Scene dialog = new Scene(roopGroup, 300, 450, Color.TRANSPARENT);
        initStyle(StageStyle.TRANSPARENT);
        setScene(dialog);
        HBox buttons = HBoxBuilder.create().spacing(30).build();
        buttons.setAlignment(Pos.CENTER);
        buttons.getChildren().addAll(ok, cancel);
        Label dataLabel = ViewStyle.getStyledLabel("Дата");
        Label titleLabel = ViewStyle.getStyledLabel("Название");
        Label descriptionLabel = ViewStyle.getStyledLabel("Описание");
        Label moneyLabel = ViewStyle.getStyledLabel("Сумма денег");
        HBox.setHgrow(fields, Priority.ALWAYS);
        fields.getChildren().addAll(dataLabel, dateField, titleLabel, titleField, moneyLabel, moneyField, descriptionLabel, descriptionField);
        parent.getChildren().addAll(fields, buttons);
        dateField.setMinHeight(20);
        dateField.setMinWidth(280);
        dateField.setStyle("-fx-font-size: 15px");
        dateField.setValue(LocalDate.parse(transaction.getDate(), DateTimeFormatter.ISO_LOCAL_DATE));
        titleField.setText(transaction.getTitle());
        moneyField.setText(transaction.getMoney().toString());
        descriptionField.setText(transaction.getDescription());
        ok.setOnAction(action -> {
            fillModelFields();
            okPressed = true;
            instance.close();
        });
        cancel.setOnAction(action -> {
            okPressed = false;
            instance.close();
        });
    }

    protected void fillModelFields() {
        transaction.setDate(dateField.getValue().format(DateTimeFormatter.ISO_LOCAL_DATE));
        transaction.setTitle(titleField.getText());
        transaction.setMoney(Long.parseLong(moneyField.getText()));
        transaction.setDescription(descriptionField.getText());
    }
}
