package finance.view.tab;

import finance.FinanceUtils;
import finance.service.DataService;
import finance.wrappers.Expense;
import javafx.collections.FXCollections;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tab;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.time.Month;
import java.time.format.TextStyle;
import java.util.*;

public class ChartsTab2 extends Tab implements LazyTab {

    DataService dataService = DataService.getInstance();
    final CategoryAxis xAxis = new CategoryAxis();
    final NumberAxis yAxis = new NumberAxis();
    final LineChart<String, Number> chart = new LineChart(xAxis, yAxis);
    final Map<String, XYChart.Series> serieses = new HashMap();

    public ChartsTab2() {
        setText("График 2");
        xAxis.setLabel("Месяц");
        yAxis.setLabel("Деньги");
        xAxis.setCategories(FXCollections.observableArrayList(FinanceUtils.getMonthRanges()));
        HBox box = new HBox();
        box.getChildren().add(chart);
        HBox.setHgrow(chart, Priority.ALWAYS);
        box.getChildren().add(createCheckBox());
        setContent(box);
        for (String type : Expense.Type.getTypeList()) {
            XYChart.Series<String, Number> series = new XYChart.Series();
            series.setName(type);
            serieses.put(type, series);
        }
    }

    private VBox createCheckBox() {
        VBox box = new VBox();
        for (final String type : Expense.Type.getTypeList()) {
            final CheckBox check = new CheckBox(type);
            check.setOnAction(action -> {
                if (check.isSelected()) {
                    addSeries(type);
                } else {
                    removeSeries(type);
                }
            });
            box.getChildren().add(check);
        }
        return box;
    }

    private void addSeries(String type) {
        chart.getData().add(serieses.get(type));
    }

    private void removeSeries(String type) {
        chart.getData().remove(serieses.get(type));
    }

    private static Map sortByComparator(Map<String, Map> unsortMap) {

        List<String> list = new LinkedList(unsortMap.keySet());

        Collections.sort(list, (o1, o2) -> {
            String split1[] = o1.toString().split("-");
            String split2[] = o2.toString().split("-");
            int m1 = Integer.parseInt(split1[1]);
            int m2 = Integer.parseInt(split2[1]);
            int y1 = Integer.parseInt(split1[0]);
            int y2 = Integer.parseInt(split2[0]);
            if (Integer.compare(y1, y2) == 0) {
                return Integer.compare(m1, m2);
            }
            return Integer.compare(y1, y2);
        });

        Map sortedMap = new LinkedHashMap();
        for (String key : list) {
            sortedMap.put(key, unsortMap.get(key));
        }
        return sortedMap;
    }

    @Override
    public void select() {
        Map<String, String> dateConverterMap = new HashMap();
        Map<String, Map> data = dataService.getExpenseByTypeAndMonth();
        for (String date : data.keySet()) {
            String[] split = date.split("-");
            int monthNumber = Integer.parseInt(split[1]);
            String monthName = Month.of(monthNumber).getDisplayName(TextStyle.FULL_STANDALONE, Locale.getDefault());
            dateConverterMap.put(date, monthName + " " + split[0]);
        }
        data = sortByComparator(data);
        for (String type : serieses.keySet()) {
            for (String date : data.keySet()) {
                Object value = data.get(date).get(type);
                if (value != null) {
                    serieses.get(type).getData().add(new XYChart.Data<String, Number>(dateConverterMap.get(date), (Number) value));
                } else {
                    serieses.get(type).getData().add(new XYChart.Data<String, Number>(dateConverterMap.get(date), 0));
                }
            }
        }
    }

}
