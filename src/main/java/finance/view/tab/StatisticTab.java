package finance.view.tab;

import finance.FinanceUtils;
import finance.service.*;
import finance.view.ViewStyle;
import finance.wrappers.Expense;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.GridPaneBuilder;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

import java.time.LocalDate;
import java.util.*;

public class StatisticTab extends Tab implements LazyTab {

    DataService dataService = DataService.getInstance();

    Text totalIncomeText = new Text("0");
    Text totalExpenseText = new Text("0");
    Text totalBalanceText = new Text("0");
    Text totalIncomeTextP = new Text("100%");
    Text totalExpenseTextP = new Text("0");
    Text totalBalanceTextP = new Text("0");
    List<Text> typeText = new ArrayList<>();
    List<Text> typeTextP = new ArrayList<>();
    HBox hBox = new HBox();
    PieChart chart = new PieChart();
    GridPane pane;
    ComboBox select;

    public StatisticTab() {
        setText("Общая статистика");
        pane = GridPaneBuilder.create().hgap(5).vgap(5).gridLinesVisible(false).build();
        hBox.getChildren().addAll(pane, chart);
        setContent(hBox);
        pane.setStyle("-fx-background-color:         #000000,\n" +
                "        linear-gradient(#d5ea8a, #7d8f22),\n" +
                "        linear-gradient(#9eb751, #70750d),\n" +
                "        linear-gradient(#aba60a, #23683a);");
        Text totalIncome = ViewStyle.getArialBold20Text("Общий доход :");
        Text totalExpense = ViewStyle.getArialBold20Text("Общий расход :");
        Text totalBalance = ViewStyle.getArialBold20Text("Остаток :");
        pane.setPadding(new Insets(0, 10, 0, 10));
        pane.add(getFilter(), 0, 0, 2, 1);
        pane.addRow(1, totalIncome, totalIncomeText, totalIncomeTextP);
        pane.addRow(2, totalExpense, totalExpenseText, totalExpenseTextP);
        pane.addRow(3, totalBalance, totalBalanceText, totalBalanceTextP);
        pane.addRow(4, new Separator(Orientation.HORIZONTAL));
        int i = 5;
        for (String type : Expense.Type.getTypeList()) {
            Text text = new Text(type + " :");
            Text textSum = new Text("0");
            Text textP = new Text("0");
            pane.add(text, 0, i);
            pane.add(textSum, 1, i);
            pane.add(textP, 2, i);
            typeText.add(textSum);
            typeTextP.add(textP);
            GridPane.setHalignment(text, HPos.RIGHT);
            i++;
        }

        GridPane.setHalignment(totalIncome, HPos.RIGHT);
        GridPane.setHalignment(totalExpense, HPos.RIGHT);
        GridPane.setHalignment(totalBalance, HPos.RIGHT);
    }

    private ComboBox getFilter() {
        select = ComboBoxBuilder.create().build();
        select.getItems().add("За всё время");
        select.getItems().addAll(FXCollections.observableList(FinanceUtils.getMonthRanges()));
        select.getSelectionModel().selectLast();
        select.setOnAction(action -> {
            String selected = select.getSelectionModel().getSelectedItem().toString();
            totalRefresh(FinanceUtils.dateRangeMap.get(selected));
        });
        return select;
    }

    public void totalRefresh(LocalDate date) {
        Map<String, Long> sumByType;
        Long expenseSum;
        Long incomeSum;
        if (date == null) {
            sumByType = dataService.getSumByType();
            incomeSum = dataService.getIncomeSum();
        } else {
            sumByType = dataService.getSumByTypeByMonth(date);
            incomeSum = dataService.getIncomeSum(date);
        }
        expenseSum = sumByType.get("sum");
        Long balance = incomeSum - expenseSum;

        totalIncomeText.setText(FinanceUtils.formatMoney(incomeSum));
        totalExpenseText.setText(FinanceUtils.formatMoney(expenseSum));
        totalExpenseTextP.setText(FinanceUtils.getPercent(expenseSum, incomeSum));
        totalBalanceText.setText(FinanceUtils.formatMoney(balance));
        totalBalanceTextP.setText(FinanceUtils.getPercent(balance, incomeSum));

        int i = 0;
        List<PieChart.Data> chartList = new ArrayList<>(10);
        chartList.add(new PieChart.Data("Осталось", balance));
        for (String type : Expense.Type.getTypeList()) {
            Long aLong = sumByType.get(type);
            typeText.get(i).setText(FinanceUtils.formatMoney(aLong));
            typeTextP.get(i).setText(FinanceUtils.getPercent(aLong, incomeSum));
            chartList.add(new PieChart.Data(type, aLong));
            i++;
        }
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(chartList);
        chart.setData(pieChartData);
    }

    @Override
    public void select() {
        totalRefresh(LocalDate.now());
    }
}
