package finance.view.tab;

import finance.FinanceUtils;
import finance.service.DataService;
import javafx.collections.FXCollections;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Tab;

import java.time.LocalDate;
import java.util.Map;

public class ChartsTab extends Tab implements LazyTab {

    private DataService dataService = DataService.getInstance();
    private XYChart.Series<String, Number> incomeSeries;
    private XYChart.Series<String, Number> expenseSeries;

    public ChartsTab() {
        setText("График");
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final BarChart<String, Number> chart = new BarChart(xAxis, yAxis);
        xAxis.setLabel("Месяц");
        yAxis.setLabel("Деньги");
        xAxis.setCategories(FXCollections.observableArrayList(FinanceUtils.getMonthRanges()));

        incomeSeries = new XYChart.Series();
        expenseSeries = new XYChart.Series();
        incomeSeries.setName("Доходы");
        expenseSeries.setName("Расходы");
        chart.getData().add(incomeSeries);
        chart.getData().add(expenseSeries);
        setContent(chart);
    }


    @Override
    public void select() {
        Map<String, LocalDate> dateRangeMap = FinanceUtils.dateRangeMap;
        incomeSeries.getData().clear();
        expenseSeries.getData().clear();
        Map<String, Long> incomeSumMap = dataService.getIncomeDAO().getSumMap();
        Map<String, Long> expenseSumMap = dataService.getExpenseDAO().getSumMap();
        for (String key : dateRangeMap.keySet()) {
            LocalDate date = dateRangeMap.get(key);
            String id = FinanceUtils.dateFormater.format(date);;
            try {
                long incomeSum = incomeSumMap.get(id);
                long expenseSum = expenseSumMap.get(id);
                incomeSeries.getData().add(new XYChart.Data(key, incomeSum));
                expenseSeries.getData().add(new XYChart.Data(key, expenseSum));
            }catch (Exception e){
                System.out.println(id);
            }
        }
    }
}
