package finance.view.tab;

import finance.FinanceUtils;
import finance.service.DataService;
import finance.view.ViewStyle;
import finance.view.dialog.ExpenseDialogScene;
import finance.view.table.ExpenseTable;
import finance.wrappers.Expense;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;

import java.time.LocalDate;
import java.util.List;

public class ExpenseTab extends Tab implements LazyTab{

    protected DataService dataService = DataService.getInstance();
    protected TableView table;
    private ComboBox select;

    public ExpenseTab() {
        setText("Расходы");
        VBox vBox = VBoxBuilder.create().spacing(10).build();
        table = new ExpenseTable();
        vBox.getChildren().add(getFilterBox());
        vBox.getChildren().add(table);
        vBox.getChildren().add(getButtonBox());
        setContent(vBox);
        List data = dataService.getExpenseByMonth(LocalDate.now());
        table.setItems(FXCollections.observableList(data));
    }

    private HBox getFilterBox() {
        HBox filterBox = new HBox();
        select = ComboBoxBuilder.create().prefWidth(200).build();
        select.getItems().add("Все доходы");
        select.getItems().addAll(FXCollections.observableList(FinanceUtils.getMonthRanges()));
        select.getSelectionModel().selectLast();
        select.setOnAction(getSelectAction());
        filterBox.getChildren().add(select);
        filterBox.setStyle("-fx-background-color: dimgrey");
        return filterBox;
    }

    private HBox getButtonBox() {
        Button add = ViewStyle.getStyledMenuButton("Добавить");
        Button edit = ViewStyle.getStyledMenuButton("Изменить");
        Button remove = ViewStyle.getStyledMenuButton("Удалить");
        add.setOnAction(getAddAction());
        edit.setOnAction(getEditAction());
        remove.setOnAction(getRemoveAction());
        HBox hbox = HBoxBuilder.create().spacing(20).build();
        hbox.setAlignment(Pos.CENTER);
        hbox.setStyle("-fx-background-color: dimgrey");
        hbox.getChildren().addAll(add, edit, remove);
        return hbox;
    }

    private EventHandler<ActionEvent> getSelectAction() {
        return action -> {
            String selected = select.getSelectionModel().getSelectedItem().toString();
            LocalDate date = FinanceUtils.dateRangeMap.get(selected);
            if (date == null) {
                table.setItems(FXCollections.observableList(dataService.getExpenseDAO().readAll()));
            } else {
                table.setItems(FXCollections.observableList(dataService.getExpenseByMonth(date)));
            }
        };
    }

    private EventHandler<ActionEvent> getAddAction() {
        return action -> {
            final Expense expense = new Expense();
            ExpenseDialogScene dialog = getDialog(expense);
            dialog.showAndWait();
            if (dialog.isOK()) {
                DataService.getInstance().getExpenseDAO().save(expense);
                table.getItems().add(expense);
            }
        };
    }

    private EventHandler<ActionEvent> getEditAction() {
        return action -> {
            Expense expense = (Expense) table.getSelectionModel().getSelectedItem();
            if (expense == null) return;
            ExpenseDialogScene stage = getDialog(expense);
            stage.showAndWait();
            if (stage.isOK()) {
                DataService.getInstance().getExpenseDAO().update(expense);
                TableColumn column = (TableColumn) table.getColumns().get(0);
                column.setVisible(false);
                column.setVisible(true);
            }
        };
    }

    private EventHandler<ActionEvent> getRemoveAction() {
        return action -> {
            Expense expense = (Expense) table.getSelectionModel().getSelectedItem();
            if (expense == null) return;
            dataService.getExpenseDAO().delete(expense);
            table.getItems().remove(expense);
        };
    }

    protected ExpenseDialogScene getDialog(Expense expense) {
        return new ExpenseDialogScene(expense);
    }

    @Override
    public void select() {

    }
}
