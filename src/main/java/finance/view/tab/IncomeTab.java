package finance.view.tab;

import finance.FinanceUtils;
import finance.service.DataService;
import finance.view.ViewStyle;
import finance.view.dialog.IncomeDialogStage;
import finance.view.table.IncomeTable;
import finance.wrappers.Income;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;

import java.time.LocalDate;
import java.util.List;

public class IncomeTab extends Tab implements LazyTab {

    protected DataService dataService = DataService.getInstance();

    protected TableView table;
    protected ComboBox select;
    protected boolean isInit;

    protected IncomeDialogStage getDialog(Income income) {
        return new IncomeDialogStage(income);
    }

    public IncomeTab() {
        setText("Доходы");
        VBox vBox = VBoxBuilder.create().spacing(0).build();
        table = new IncomeTable();
        vBox.getChildren().add(getFilterBox());
        vBox.getChildren().add(table);
        vBox.getChildren().add(getButtonBox());
        setContent(vBox);
    }

    private HBox getFilterBox() {
        HBox filterBox = new HBox();
        select = ComboBoxBuilder.create().prefWidth(200).build();
        select.getItems().add("Все доходы");
        select.getItems().addAll(FXCollections.observableList(FinanceUtils.getMonthRanges()));
        select.getSelectionModel().selectLast();
        select.setOnAction(getSelectAction());
        filterBox.getChildren().add(select);
        filterBox.setStyle("-fx-background-color: dimgrey");
        return filterBox;
    }

    private HBox getButtonBox() {
        Button add = ViewStyle.getStyledMenuButton("Добавить");
        Button edit = ViewStyle.getStyledMenuButton("Изменить");
        Button remove = ViewStyle.getStyledMenuButton("Удалить");
        add.setOnAction(getAddAction());
        edit.setOnAction(getEditAction());
        remove.setOnAction(getRemoveAction());
        HBox hbox = HBoxBuilder.create().spacing(20).build();
        hbox.setAlignment(Pos.CENTER);
        hbox.setStyle("-fx-background-color: dimgrey");
        hbox.getChildren().addAll(add, edit, remove);
        return hbox;
    }

    private EventHandler<ActionEvent> getSelectAction() {
        return action -> {
            String selected = select.getSelectionModel().getSelectedItem().toString();
            LocalDate date = FinanceUtils.dateRangeMap.get(selected);
            if (date == null) {
                table.setItems(FXCollections.observableList(dataService.getIncomeDAO().readAll()));
            } else {
                table.setItems(FXCollections.observableList(dataService.getIncomeByMonth(date)));
            }
        };
    }

    private EventHandler<ActionEvent> getAddAction() {
        return action -> {
            final Income income = new Income();
            IncomeDialogStage dialog = getDialog(income);
            dialog.showAndWait();
            if (dialog.isOK()) {
                dataService.getIncomeDAO().save(income);
                table.getItems().add(income);
            }
        };
    }

    private EventHandler<ActionEvent> getEditAction() {
        return action -> {
            Income income = (Income) table.getSelectionModel().getSelectedItem();
            if (income == null) return;
            IncomeDialogStage stage = getDialog(income);
            stage.showAndWait();
            if (stage.isOK()) {
                dataService.getIncomeDAO().update(income);
                TableColumn column = (TableColumn) table.getColumns().get(0);
                column.setVisible(false);
                column.setVisible(true);
            }
        };
    }

    private EventHandler<ActionEvent> getRemoveAction() {
        return action -> {
            Income income = (Income) table.getSelectionModel().getSelectedItem();
            if (income == null) return;
            dataService.getIncomeDAO().delete(income);
            table.getItems().remove(income);
        };
    }

    @Override
    public void select() {
        if (!isInit) {
            List data = dataService.getIncomeByMonth(LocalDate.now());
            table.setItems(FXCollections.observableList(data));
            isInit = true;
        }
    }
}
