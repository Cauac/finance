package finance.view;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextBuilder;


public class ViewStyle {

    static String buttonCss = ViewStyle.class.getResource("/css/button.css").toExternalForm();
    static Font font = Font.font("Arial", FontWeight.BOLD, 20);


    public static Node getDialogBackground(int width, int height) {
        RadialGradient gr = new RadialGradient(-0.3, 155, 0.5, 0.5, 1, true, CycleMethod.NO_CYCLE, new Stop[]{
                new Stop(0, Color.DARKGRAY),
                new Stop(1, Color.BLACK)
        });
        Rectangle r = new Rectangle(0, 0, width + 5, height + 5);
        r.setArcHeight(40);
        r.setArcWidth(40);
        r.setFill(gr);
        return r;
    }

    public static Label getStyledLabel(String text) {
        return LabelBuilder.create().textFill(Color.AZURE).text(text).alignment(Pos.TOP_CENTER).font(new Font(20)).build();
    }

    public static TextField getStyledTextFiled() {
        TextField fiel = TextFieldBuilder.create().minHeight(20).build();
        fiel.setStyle("-fx-font-size: 15px");
        return fiel;
    }

    public static Text getArialBold20Text(String text) {
        return TextBuilder.create().text(text).font(font).build();
    }

    public static Button getStyledMenuButton(String text) {
        Button b = new Button(text);
        b.getStylesheets().add(buttonCss);
        return b;
    }


}
