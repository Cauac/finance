package finance.view.table;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;

public class ExpenseTable extends IncomeTable {

    public ExpenseTable() {
        TableColumn type = new TableColumn();
        type.setText("Тип");
        type.setCellValueFactory(new PropertyValueFactory("type"));
        getColumns().add(1, type);
    }
}
