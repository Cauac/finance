package finance.view.table;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class IncomeTable extends TableView {

    public IncomeTable() {
        VBox.setVgrow(this, Priority.ALWAYS);
        TableColumn date = new TableColumn();
        date.setText("Дата");
        date.setCellValueFactory(new PropertyValueFactory("date"));

        TableColumn title = new TableColumn();
        title.setText("Название");
        title.setCellValueFactory(new PropertyValueFactory("title"));

        TableColumn money = new TableColumn();
        money.setText("Сумма денег");
        money.setCellValueFactory(new PropertyValueFactory("money"));

        TableColumn description = new TableColumn();
        description.setText("Описание");
        description.setCellValueFactory(new PropertyValueFactory("description"));

        setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        getColumns().addAll(date, title, money, description);
    }
}
